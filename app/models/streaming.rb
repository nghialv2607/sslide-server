class Streaming < ActiveRecord::Base
  attr_accessible :username,
                  :channel,
                  :slideId,
                  :title,
                  :thumbnailUrl,
                  :numViews,
                  :numDownloads,
                  :numFavorites,
                  :created,
                  :totalSlides,
                  :slideImageBaseurl,
                  :slideImageBaseurlSuffix,
                  :firstPageImageUrl
end
