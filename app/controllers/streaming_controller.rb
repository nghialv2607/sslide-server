class StreamingController < ApplicationController
skip_before_filter :verify_authenticity_token

  def search
    streaming = Streaming.where(username: params[:username])
    render json: streaming
  end

  def create
    Streaming.destroy_all(username: params[:username])
    streaming = Streaming.new(params)
    streaming.save
    render json: {result: "OK"}
  end

  def remove
    Streaming.destroy_all(channel: params[:channel])
    render json: {result: "OK"}
  end

end
