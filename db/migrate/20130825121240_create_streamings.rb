class CreateStreamings < ActiveRecord::Migration
  def change
    create_table :streamings do |t|
      t.string :username
      t.string :channel
      t.string :slideId
      t.string :title
      t.string :thumbnailUrl
      t.string :created
      t.integer :numViews
      t.integer :numDownloads
      t.integer :numFavorites
      t.integer :totalSlides
      t.string :slideImageBaseurl
      t.string :slideImageBaseurlSuffix
      t.string :firstPageImageUrl

      t.timestamps
    end
  end
end
